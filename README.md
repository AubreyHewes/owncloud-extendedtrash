# OwnCloud Extended Trash

Extends OwnCloud trash functionality with extra (missing) functionality.

Status: WIP

## Description:
This OwnCloud App supports the following:

 * Admin can clean user trash based on a filter (not yet complete; removes oc_trash* but does not yet remove
 deleted files from oc_filecache). As the cache is not removed the deleted files button is available..
 and when selecting deleted files (clicking the button) the cached files are loaded then removed..

# Why?
Due to previous errors within the server/client architecture where files were not being deleted. The amount of
user trashfiles were extremely large.
Waiting for the retention to clean the files took too long (even after reducing to 30 days).
The user's could also no longer delete the trash due to the amount of files and misc/network timeouts. Thus
requesting admin to remove the files for them; currently admin can not do this nicely (via the api).

## Problem
Retention of trashfiles can bloat the storage medium. Admin needs to always be able to clean user trashfiles.
Users need to be able to remove trash by filter.

### Requirement
 * Admin should be able to force cleaning of user trash.
 * User should be able to force cleaning of trash via filter.

