<?php
/* @var array $param */

OCP\JSON::checkLoggedIn();
OCP\JSON::checkAppEnabled($param['app']);
OCP\JSON::callCheck();

$action = isset($_POST['action']) ? $_POST['action'] : $_GET['action'];
$user = isset($_POST['user']) ? $_POST['user'] : $_GET['user'];
$dateFrom = isset($_POST['dateFrom']) ? $_POST['dateFrom'] : $_GET['dateFrom'];
$dateTill = isset($_POST['dateTill']) ? $_POST['dateTill'] : $_GET['dateTill'];

// check that the user can clean (i.e. is own user; else is admin)
if (OCP\User::getUser() !== $user || "all" == $user) {
	OCP\JSON::checkAdminUser();
}

/**
 * Do stuff on user/all
 */
function doClean($user, $dateFrom, $dateTill) {
	$params = array();

	$dateFrom = strtotime($dateFrom);
	$dateTill = strtotime($dateTill);

	$pstmt = 'DELETE FROM `*PREFIX*files_trash`';

	if ($user !== 'all') {
		$pstmt .= ' WHERE `user`=?';
		$params[] = $user;
	}
	if (!empty($dateFrom)) {
		$pstmt .= ' AND `timestamp` >= ?';
		$params[] = $dateFrom;
	}
	if (!empty($dateTill)) {
		$pstmt .= ' AND `timestamp` <= ?';
		$params[] = $dateTill;
	}

	$query = \OC_DB::prepare($pstmt);

	$result = $query->execute($params);

	if ($user !== 'all') {
		setTrashbinSize($user, getTrashbinSize($user));
	} else {
		foreach (OCP\User::getUsers() as $user) {
			setTrashbinSize($user, getTrashbinSize($user));
		}
	}
	return $result;
}

/**
 * @copy from trashbin.php
 *
 * get current size of trash bin from a given user
 *
 * @param $user user who owns the trash bin
 * @return mixed trash bin size or false if no trash bin size is stored
 */
function getTrashbinSize($user) {
	$query = \OC_DB::prepare('SELECT `size` FROM `*PREFIX*files_trashsize` WHERE `user`=?');
	$result = $query->execute(array($user))->fetchAll();
	if ($result) {
		return (int)$result[0]['size'];
	}
	return false;
}

function setTrashbinSize($user, $size) {
	if (getTrashbinSize($user) === false) {
		$query = \OC_DB::prepare('INSERT INTO `*PREFIX*files_trashsize` (`size`, `user`) VALUES (?, ?)');
	} else {
		$query = \OC_DB::prepare('UPDATE `*PREFIX*files_trashsize` SET `size`=? WHERE `user`=?');
	}
	$query->execute(array($size, $user));
}


$result=false;
switch($action) {
	case 'clean':
		$result = doClean($user, $dateFrom, $dateTill);
		break;
}

OC_JSON::success(array('data'=>$result));
