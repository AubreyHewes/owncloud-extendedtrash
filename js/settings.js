(function(){
	var OCA = OCA || {};
	OCA.ExtendedTrash = OCA.ExtendedTrash || {
		url: OC.filePath('extendedtrash','ajax','trash.php'),
		postCall: function (action, data, callback){
			data.action = action;
			$.post(OCA.ExtendedTrash.url, data, function (result){
				if(result.status === 'success'){
					if(callback){
						callback(result.data);
					}
				}
			},'json');
		},
		apply: function ($container) {
			var params = {};
			params.user = $container.find('#user').val();
			params.dateFrom = $container.find('#dateFrom').val();
			params.dateTill = $container.find('#dateTill').val();
			OCA.ExtendedTrash.postCall('clean', params, function () {
				console.log('done');
			});
		}
	};

	$(document).ready(function() {
		$('#oca-extendedtrash.settings').on('click', '#apply', function(event) {
			OCA.ExtendedTrash.apply($(event.delegateTarget));

		}).find('.datepicker').datepicker({

		});
	});

})();