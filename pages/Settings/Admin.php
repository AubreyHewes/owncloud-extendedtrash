<?php
OC_Util::checkAdminUser();

OCP\Util::addScript('extendedtrash', 'settings');
//OCP\Util::addStyle('extendedtrash', 'settings');

$tmpl = new OCP\Template('extendedtrash', 'Settings/Admin');
$tmpl->assign('users', OCP\User::getUsers());
return $tmpl->fetchPage();
