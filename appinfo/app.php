<?php
namespace OCA\ExtendedTrash;

///////////////////////////////////////////////
// create app

// need these for initial classloading
\OC::$CLASSPATH['OCA\ExtendedTrash\AbstractApp'] = 'apps/extendedtrash/lib/AbstractApp.php';
\OC::$CLASSPATH['OCA\ExtendedTrash\App'] = 'apps/extendedtrash/lib/App.php';

// this does the rest
new App('extendedtrash');
