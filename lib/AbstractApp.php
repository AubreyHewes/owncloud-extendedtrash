<?php
namespace OCA\ExtendedTrash;

/**
 * Class AbstractApp
 * @package OCA\TrashCleaner
 */
abstract class AbstractApp /* extends \OC\App (and/or) implements \OC\App */ {

	/**
	 * @var string
	 */
	private $name;

	/**
	 * @var \OCA\AppFramework\Core\API
	 */
	private $api;

	/**
	 * @param string $name
	 */
	public function __construct($name) {
		if(!\OCP\App::isEnabled('appframework')){
			\OCP\Util::writeLog($name,
				'Can not enable the "' . $name . '" app because the "App Framework" app is disabled',
				\OCP\Util::ERROR);
			return;
		}
		$this->name = $name;
		$this->api = new \OCA\AppFramework\Core\API($this->name);
		$this->initClassMappings();
		$this->initAdminSettings();
		$this->initPersonalSettings();
		$this->init();
	}

	/**
	 * sets the required class-mappings
	 * @todo interface?
	 */
	private function initClassMappings() {
		foreach ($this->getClassMappings() as $name => $file) {
			\OC::$CLASSPATH[$name] = $file;
		}
	}

	/**
	 * @return array
	 */
	public abstract function getClassMappings();

	/**
	 * sets the setting screens
	 */
	private function initAdminSettings() {
		foreach ($this->getAdminSettings() as $name) {
			\OCP\App::registerAdmin($this->getApi()->getAppName(), $name);
		}
	}

	/**
	 * @return array
	 */
	public abstract function getAdminSettings();

	/**
	 * sets the setting screens
	 * @todo interface?
	 */
	private function initPersonalSettings() {
		foreach ($this->getPersonalSettings() as $name) {
			\OCP\App::registerPersonal($this->getApi()->getAppName(), $name);
		}
	}

	/**
	 * @return array
	 */
	public abstract function getPersonalSettings();

	/**
	 * @return mixed
	 */
	public abstract function init();

	/**
	 * @param \OCA\AppFramework\Core\API $api
	 */
	public function setApi($api) {
		$this->api = $api;
	}

	/**
	 * @return \OCA\AppFramework\Core\API
	 */
	public function getApi() {
		return $this->api;
	}


}