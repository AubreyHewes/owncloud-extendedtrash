<?php
namespace OCA\ExtendedTrash;

/**
 * Class App
 * @package OCA\TrashCleaner
 */
class App extends AbstractApp {

	/**
	 * @override
	 * @return array
	 */
	public function getAdminSettings() {
		return array('pages/Settings/Admin');
	}

	/**
	 * @override
	 * @return array
	 */
	public function getPersonalSettings() {
		return array(/*'pages/Settings/User'*/);
	}

	/**
	 * @override
	 * @return array
	 */
	public function getClassMappings() {
		return array();
	}

	/**
	 * @override
	 */
	public function init() {
	}

}