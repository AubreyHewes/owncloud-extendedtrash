<?php
/* @var OC_L10N $l */
/* @var array $_ */
?><form id="oca-extendedtrash" class="settings settings-user" role="form">
	<fieldset class="personalblock">
		<h2><?php p($l->t('Trash Cleaner')); ?></h2>

		<div class="alert alert-notice">
			<?php print_unescaped($l->t('Have problems/issues with this application? <a target="_blank" href="https://bitbucket.org/AubreyHewes/owncloud-extendedtrash/">add them here</a>!')); ?>
		</div>

		<div class="alert alert-warning">
			<?php p($l->t('The following settings will only be applied if the theme does not contain it\'s own defaults.')); ?>
		</div>

	</fieldset>
</form>

