<?php
/* @var OC_L10N $l */
/* @var array $_ */
?><form id="oca-extendedtrash" class="settings settings-admin" role="form">
	<fieldset class="personalblock">
	<h2><?php p($l->t('Trash Cleaner')); ?></h2>

		<div class="alert alert-notice">
			<?php print_unescaped($l->t('Have problems/issues with this application? <a target="_blank" href="https://bitbucket.org/AubreyHewes/owncloud-extendedtrash/">add them here</a>!')); ?>
		</div>

		<div class="form-group">
			<label class="control-label" for="theme"><?php p($l->t('User')); ?></label>
			<div>
				<select class="form-control" name="user" id="user">
					<option value="all"><?php p($l->t('All users')); ?></option>
				<?php foreach ($_['users'] as $user): ?>
					<option value="<?php p($user); ?>"><?php p($user); ?></option>
				<?php endforeach; ?>
				</select>
				<span class="help-block"><?php p($l->t('Remove trash for this user')); ?></span>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label" for="from"><?php p($l->t('From / Till')); ?></label>
			<div>
				<input type="text" name="fromDate" id="fromDate" class="datepicker"/>
				<input type="text" name="toDate" id="toDate" class="datepicker"/>
				<span class="help-block"><?php p($l->t('Remove trash for selected user(s)')); ?></span>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label"></label>
			<div>
				<input type="button"
					   name="apply"
					   id="apply"
					   value="<?php p($l->t('Apply')); ?>"/>
				<br/>
				<span class="help-block"><?php p($l->t('Remove matching trash files')); ?></span>
			</div>
		</div>

	</fieldset>
</form>

